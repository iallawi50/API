

// GET
async function info()
{
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');

    const data = await response.json();

    console.log(data);

    document.querySelector('#content h1').innerHTML = data[0].title;
    document.querySelector('#content p').innerHTML = data[0].body;
}

info()

//POST
var form  = document.getElementById("form");
        form.addEventListener("submit" , function(e){
            e.preventDefault()
            var name = document.getElementById("name").value
            var postt = document.getElementById("post").value
            fetch("https://jsonplaceholder.typicode.com/posts",{
                method : "POST",
                body:JSON.stringify({
                     title: name,
                     body: postt
                    
                }),
                headers:{
                    "Content-Type":"application/json; charset=UTF-8"
                }
            })
            .then(function(response){
                return response.json()
            })
            .then(function(data){
                console.log(data)
            })

        })

        // UPDATE
        var up  = document.getElementById("update");
        up.addEventListener("click" , function(e){
            e.preventDefault()
            var name = document.getElementById("name").value
            var postt = document.getElementById("post").value
            fetch("https://jsonplaceholder.typicode.com/posts",{
                method : "PUT",
                body:JSON.stringify({
                     title: name,
                     body: postt
                    
                }),
                headers:{
                    "Content-Type":"application/json; charset=UTF-8"
                }
            })
            .then(function(response){
                return response.json()
            })
            .then(function(data){
                console.log(data)
            })

        })

// DELETE
        function xDelete()
        {
            fetch('https://jsonplaceholder.typicode.com/posts/101', {
                method: 'DELETE',
              })
              .then(res => res.text()) // or res.json()
              .then(res => console.log(res))
        }
        
        



























